# nibble

> git tool to create bite-sized branches from a source. Useful for making smaller MRs.

```
> nibble <path> [<path>...]
```

_Note: `path` can be a comma seperated string of paths to aggregate into a single nibble_

Examples:

```
> git branch
* our-large-feature

> nibble ./frontend ./backend
[nibble] Created branches `our-large-feature-frontend-nibble` and `our-large-feature-backend-nibble`.
[nibble] Committed all changes.
```

The `our-large-feature-frontend-nibble` branch will contain all the changes under the `./frontend` directory
and changes under the `./backend` directory will be contained in the `our-large-feature-backend-nibble` branch.
Changes under both these directories will also be removed from the source branch. (In the example above,
the source branch would be the `our-large-feature` branch)

You can also aggregate multiple paths into single nibbles:

```
> nibble ./frontend,./tests/frontend ./backend,./tests/backend
```
